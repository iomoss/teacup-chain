all: Teacup_Firmware/sim

Teacup_Firmware/Makefile-SIM:
	git clone https://github.com/Traumflug/Teacup_Firmware.git

teacup_config/config.h:
	git clone https://skeen@bitbucket.org/sculptor_dev/teacup_config.git

Teacup_Firmware/config.h: Teacup_Firmware/Makefile-SIM teacup_config/config.h
	cp teacup_config/config.h Teacup_Firmware/config.h
	cp teacup_config/thermistortable.h Teacup_Firmware/thermistortable.h
#	echo "Please setup the printer for firmware compilation"
#	cd Teacup_Firmware && python configtool.py

Teacup_Firmware/sim: Teacup_Firmware/config.h
	cd Teacup_Firmware && make -f Makefile-SIM

sculptor-preprocess/README.md:
	git clone https://skeen@bitbucket.org/sculptor_dev/sculptor-preprocess.git

SpinSim/README.md:
	git clone https://github.com/Skeen/SpinSim.git

build/PLACEHOLDER:
	mkdir -p build/
	echo "Do not delete" > build/PLACEHOLDER

build/preprocessed.gcode: sculptor-preprocess/README.md sculptor-preprocess/process.lua build/PLACEHOLDER input.gcode
	cd sculptor-preprocess && lua process.lua ../input.gcode > ../build/preprocessed.gcode

Teacup_Firmware/datalog.out: Teacup_Firmware/sim build/preprocessed.gcode
	cd Teacup_Firmware && ./sim ../build/preprocessed.gcode -t 0 -o

build/datalog.out: Teacup_Firmware/datalog.out build/PLACEHOLDER
	cp Teacup_Firmware/datalog.out build/datalog.out

build/datalog.csv: build/datalog.out build/PLACEHOLDER tools/csv_maker.sh
	./tools/csv_maker.sh build/datalog.out > build/datalog.csv

.PHONY: preprocess simulate csv_datalog clean

preprocess: build/preprocessed.gcode
simulate: preprocess build/datalog.out
csv_datalog: simulate build/datalog.csv
run_render: csv_datalog SpinSim/README.md
	python SpinSim/spinsim.py build/datalog.csv

clean:
	rm -rf build

clean-all: clean
	rm -rf SpinSim
	rm -rf sculptor-preprocess
	rm -rf Teacup_Firmware
	rm -rf teacup_config
